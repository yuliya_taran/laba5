import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Map;

public class Laba {
    public static void main(String[] args)  throws CloneNotSupportedException {

        //Task1
        User user1 = new User();
        user1.setName("name1");
        user1.setLogin("login1");
        user1.setPassword("password1");
        User user2 = new User();
        user2.setName("name2");
        user2.setLogin("login2");
        user2.setPassword("password2");
        User user3 = new User();
        user3.setName("name3");
        user3.setLogin("login3");
        user3.setPassword("password3");
        System.out.println("Task1");
        System.out.println(user1.showInfo("/"));
        System.out.println(user2.showInfo(" "));
        System.out.println(user3.showInfo("\n"));

        //Task2
        User user4 = new User("name4", "login4","password4");
        User user5 = new User("name5", "login5","password5");
        User user6 = new User("name6", "login6","password6");
        System.out.println("Task2");
        System.out.println(user4.showInfo("*"));
        System.out.println(user5.showInfo("+"));
        System.out.println(user6.showInfo(" - "));


        System.gc();
        System.runFinalization ();
        //Task3
        User user7 = (User) user1.clone();
        System.out.println("Task3");
        System.out.println("Clone: " + user7.showInfo("\n"));

        //Task4
        SuperUser user8 = new SuperUser();
        user8.setName("name8");
        user8.setLogin("login8");
        user8.setPassword("password8");
        user8.setRole("admin");
        System.out.println("Task4");
        System.out.println(user8.showInfo(" "));
        System.out.println(user8.getRole());

        //Task5 - 6
        SuperUser user9 = new SuperUser("name9", "login9","password9", "admin");
        System.out.println("Task5-6");
        System.out.println(user9.showInfo(" "));
        System.out.println(user9.getRole());

        //Task7
        try {
            SuperUser user10 = new SuperUser(null, "a", "b", "c");
        } catch (IllegalArgumentException e) {
            System.out.println("Task7");
            System.out.println(e.getMessage());
        }

        //Task8
        System.out.println("Task8");
        System.out.println(SuperUser.INFO_TITLE + user9.showInfo(" "));
        System.out.println(SuperUser.showTitle() + user9.showInfo(" "));

        //Task10
        Map<String, String> userInfo = user9.getInfo();
        System.out.println("Task10");
        for (String property : userInfo.keySet()) {
            System.out.println(property + ": " + userInfo.get(property));
        }

        //Task11
        System.out.println("Task11");
        System.out.println("USER_OBJECT_QUANTITY: " + User.USER_OBJECT_QUANTITY);
        System.out.println("SUPER_USER_OBJECT_QUANTITY: " + SuperUser.SUPER_USER_OBJECT_QUANTITY);

        //Task12
        System.out.println("Task12");
        System.out.println(checkObject(user9));
        System.out.println(checkObject(user5));
        System.out.println(checkObject(new Integer(1)));

        //Task13
        System.out.println("Task13");
        System.out.println(user1.toString());
        System.out.println(user2.toString());
        System.out.println(user3.toString());
        System.out.println(user4.toString());
        System.out.println(user5.toString());
        System.out.println(user6.toString());
        System.out.println(user7.toString());
        System.out.println(user8.toString());
        System.out.println(user9.toString());

    }

    public static String checkObject(Object object){
        if (object instanceof SuperUser){
            return "User has admin role.";
        }
        return object instanceof User ? "User is regular user." : "Unknown user";
    }

}
