import com.sun.org.apache.xpath.internal.SourceTree;

public class User extends AUser implements Cloneable{

    public static final String INFO_TITLE = "Данные пользователя:";
    public static int USER_OBJECT_QUANTITY = 0;

    private String name;

    private String password;

    private String login;

    private int objNum;

    public User(){
        this.objNum = ++USER_OBJECT_QUANTITY;
    }

    public User(String name, String password, String login){
        this.objNum = ++USER_OBJECT_QUANTITY;
        this.name = name;
        this.password = password;
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String showInfo(String delimeter) {
        return name + delimeter + password + delimeter + login;
    }

    public void finalize(){
        System.out.println("destruct object" + name);
        this.name = null;
        this.password = null;
        this.login = null;
    }

    protected Object clone() throws CloneNotSupportedException{
        User user = (User)super.clone();
        user.name = user.name != null ? user.name : "Guest";
        user.login = user.login != null ? user.login : "guest";
        user.password = user.password != null ? user.password : "qwerty";
        return user;
    }

    public static String showTitle(){
        return INFO_TITLE;
    }

    public int getObjNum() {
        return objNum;
    }

    @Override
    public String toString(){
        return " Объект #" + objNum + ":" + name;
    }
}
