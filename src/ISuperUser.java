import java.util.Map;

public interface ISuperUser {
    Map<String, String> getInfo();
}
