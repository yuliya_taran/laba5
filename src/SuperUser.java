import java.util.HashMap;
import java.util.Map;

public class SuperUser extends User implements ISuperUser{
    public static int SUPER_USER_OBJECT_QUANTITY = 0;

    private String role;

    public SuperUser(){
        SUPER_USER_OBJECT_QUANTITY++;
    }
    public SuperUser(String name, String password, String login, String role){
        super(name, password, login);
        SUPER_USER_OBJECT_QUANTITY++;
        if(name == null || login == null || password == null || role == null){
            throw new IllegalArgumentException("Please fill all params");
        }
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String showInfo(String delimeter){
        return super.showInfo(delimeter) + delimeter + this.role;
    }

    @Override
    public Map<String, String> getInfo(){
        Map<String, String> userInfo = new HashMap<>();
        userInfo.put("name", getName());
        userInfo.put("password", getPassword());
        userInfo.put("login", getLogin());
        userInfo.put("role", role);
        return userInfo;
    }
}
